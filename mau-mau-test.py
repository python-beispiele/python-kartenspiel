import random

def erstelle_blatt():
    farbwerte = ["Kreuz","Pik","Herz","Karo"]
    blatt = []
    for farbwert in farbwerte:
        for i in range(2,11):
            blatt.append(farbwert + " " + str(i))
        blatt.append(farbwert + " Bube")
        blatt.append(farbwert + " Dame")
        blatt.append(farbwert + " König")
        blatt.append(farbwert + " Ass")
    return blatt

def top(stack):
    if len(stack) > 0:
        return stack[len(stack) - 1]
    else:
        return None

def is_empty(stack):
    return len(stack) == 0

def pull(stack):
    return stack.pop(random.randint(0,len(stack) - 1))

def karte_ziehen(s, h):
    oberste_karte = top(h)
    if is_empty(s):
        s = h
        s.remove(oberste_karte)
        h = [oberste_karte]
        print("Stapel umgedreht")
    return pull(s)    

blatt = erstelle_blatt()

spieler = [[], [], []]

# jeder Spieler bekommt 6 Karten
for i in range(18):
    karte = pull(blatt)
    spieler[i % 3].append(karte)

# einmal alles ausgeben
print("Blatt ", blatt)
print("S1", spieler[0])
print("S2", spieler[1])
print("S3", spieler[2])

stapel = blatt
haufen = []
zug = 0

k = pull(stapel)
haufen.append(k)

ziehen = 0
aussetzen = False
wunschfarbe = None 

# solange alle Spieler noch min. eine Karte auf der Hand haben ...
# ... und zur Sicherheit max 100 Runden ;-)
while len(spieler[0]) > 0 and len(spieler[1]) > 0 and len(spieler[2]) > 0 and zug < 100:
    zug = zug + 1
    abgelegt = False
    oberste_karte = top(haufen)
    ok = oberste_karte.split()
    if not wunschfarbe == None:
        ok[0] = wunschfarbe
    print("\n" + str(zug) + ". Zug - es liegt " + str(ok))
    print(spieler[zug % 3])

    if ok[1] == "8" and aussetzen == True:
        aussetzen = False
        continue

    # menschlicher spieler
    if zug % 3 == 0: 
        guelteiger_zug = False
        while not guelteiger_zug:
            k = input("Karte ?")
            k_wert = k.split()
            if k == "z":
                karte = karte_ziehen(stapel, haufen)
                spieler[zug % 3].append(karte)
                print("Spieler ", zug % 3 + 1, " zieht ", karte)
                guelteiger_zug = True
            elif k in spieler[zug % 3] and (k_wert[0] == ok[0] or k_wert[1] == ok[1]):
                spieler[zug % 3].remove(k)
                haufen.append(k)
                if k_wert[1] == "7":
                    ziehen = ziehen + 2

                if k_wert[1] == "8":
                    aussetzen = True
                abgelegt = True
                print("Spieler ", zug % 3 + 1, " legt ", k)
                guelteiger_zug = True
            else:
                print("ungueltiger zug!")
        continue
            
    if ok[1] == "7" and ziehen > 0:

        if "7" in "".join(spieler[zug % 3]):
            for karte in spieler[zug % 3]:
                k_wert = karte.split()  # "Pik 7" -> ["Pik","7"] 
                if k_wert[1] == ok[1]:
                    spieler[zug % 3].remove(karte)
                    haufen.append(karte)
                    abgelegt = True
                    print("Spieler ", zug % 3 + 1, " legt ", karte)
                    break

            ziehen = ziehen + 2
        else:
            for z in range(ziehen):
                karte = karte_ziehen(stapel, haufen)
                spieler[zug % 3].append(karte)
                print("Spieler ", zug % 3 + 1, " zieht " + karte)
                ziehen = 0
        continue

    # eine passende Karte ablegen, wenn vorhanden
    for karte in spieler[zug % 3]:
        k_wert = karte.split()  # "Pik 7" -> ["Pik","7"] 
        if k_wert[0] == ok[0] or k_wert[1] == ok[1]:
            spieler[zug % 3].remove(karte)
            haufen.append(karte)
            if k_wert[1] == "7":
                ziehen = ziehen + 2

            if k_wert[1] == "8":
                aussetzen = True

            abgelegt = True
            print("Spieler ", zug % 3 + 1, " legt ", karte)
            break

    # wenn keine passende Karte, aber ein Bube
    if "Bube" in "".join(spieler[zug % 3]) and abgelegt == False:
        for karte in spieler[zug % 3]:
            k_wert = karte.split()  # "Pik 7" -> ["Pik","7"] 
            if k_wert[1] == "Bube":
                spieler[zug % 3].remove(karte)
                haufen.append(karte)
                # hier noch eine gute Farbe bestimmen
                wunschfarbe = k_wert[0]
                abgelegt = True
                print("Spieler ", zug % 3 + 1, " legt ", karte)
                break

    # wenn nichts abgelegt, dann eine Karte ziehen
    if not abgelegt:
        karte = karte_ziehen(stapel, haufen)
        spieler[zug % 3].append(karte)
        print("Spieler ", zug % 3 + 1, " zieht ", karte)
    
#eingabe = input("Lege eine Karte ab: ")
print("\nHaufen ", haufen)
print("S1", spieler[0])
print("S2", spieler[1])
print("S3", spieler[2])

for i in range(3):
    if len(spieler[i]) == 0:
        print("Es gewint Spieler ", i + 1)

'''
TODO
* Bei Menschlichem Spieler 
  - Buben-Regel
  - 7 Kontern

* AUFREUMEN
'''