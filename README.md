# Python Kartenspiel

## Erstes Beispiel: Mau Mau

Ein Blatt mit 52 Karten, zunächst drei Spieler, ggf. später flexibel. Jeder Spieler bekommt 6 Karten. Karten werden der Reihe nach abgelegt, wobei einige Regeln zu beachten sind. Wer zuerst keine Karten mehr auf der Hand hat, hat gewonnen.

## Aufgabe 1

Ein Kartenspiel erstellen und ausgeben.

## Lösung 1

[Link auf Variante 1](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/508810ceb19f1bca10dc2baf51f5cf8a93e72867)
[Link auf Variante 2](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/196092871c68abf1756c70cce610acd6215094c1)

## Aufgabe 2

Zunächst drei Computerspieler modellieren und ausgeben.

## Lösung 2

[Link auf Variante 1](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/befd27c7cde7d165272ba5df71d80befb035c841)
[Link auf Variante 2](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/2780816b5785e7ce2e05929916661a9a185bc07e)

## Aufgabe 3

Jedem Spieler 6 zufällige Karten geben.

## Lösung 3

[Link auf Variante 1](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/1738bc8884e5430adfabe15f91687f80970c4cf3)
[Link auf Variante 2](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/ff20ed761d20557b7d048b614306c20d5ccecb5a)

## Aufgabe 4

Bis ein Spieler keine Karten mehr hat, legt reihum jeder eine Karte ab - zunächst ohne jegliche Logik.

## Lösung 4

[Link auf Commit](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/44a425aa1341a3c6bd4d33d7c7bdfb020f5a6176)

## Aufgabe 5

Zunächst wird eine Karte auf einen Haufen aufgedeckt. Nur gültige Züge erlauben (Farbe oder Wert müssen mit oberster Karte auf Haufen übereinstimmen). Wenn ein Spieler nichts ablegen kann, muss er eine Karte ziehen.

## Lösung 5

[Link auf Commit](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/9984b4bee7b42952fead5da0b4ccc8761d843396)

## Aufgabe 6

Manchmal tritt ein Fehler auf, wenn der Stapel leer ist bevor das Spiel entschieden ist. In diesem Fall muss der Haufen umgedreht werden und wieder eine neue Karte aufgedeckt werden.

## Lösung 6

[Link auf Commit](https://gitlab.com/python-beispiele/python-kartenspiel/-/commit/eae8d6f4768866f20334a2a2ba7fd7d056cbeda8)

## Aufgabe 7

Regeln für besondere Karten einführen: 7, 8, Bube

## Aufgabe 8

Menschlichen Spieler einführen