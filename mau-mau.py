import random

def erstelle_blatt():
    farbwerte = ["Kreuz", "Pik", "Herz", "Karo"]
    blatt = []
    for farbwert in farbwerte:
        for i in range(2,11):
            blatt.append(farbwert + " " + str(i))
        blatt.append(farbwert + " Bube")
        blatt.append(farbwert + " Dame")
        blatt.append(farbwert + " König")
        blatt.append(farbwert + " Ass")
    return blatt

def top(stack):
    if len(stack) > 0:
        return stack[len(stack) - 1]
    else:
        return None

def empty(stapel):
    return len(stapel) == 0

def pull(stack):
    return stack.pop(random.randint(0, len(stack) - 1))

blatt = erstelle_blatt()
print(blatt)

spieler = [[], [], []]
print(spieler)

for i in range(18):
    karte = pull(blatt)
    spieler[i % 3].append(karte)
print(blatt)
print(spieler)

stapel = blatt
haufen = []
zug = 0

k = pull(stapel)
haufen.append(k)

while len(spieler[0]) > 0 and len(spieler[1]) > 0 and len(spieler[2]) > 0 and zug < 1000:
    abgelegt = False
    oberste_karte = top(haufen).split()
    print("\n" + str(zug + 1) + ". Zug - es liegt " + str(oberste_karte))
    print(spieler[zug % 3])
    for k in spieler[zug % 3]:
        k_wert = k.split()
        if k_wert[0] == oberste_karte[0] or k_wert[1] == oberste_karte[1]:
            spieler[zug % 3].remove(k)
            haufen.append(k)
            oberste_karte = top(haufen)

            print("Spieler " + str(zug % 3 + 1) + " legt " + k)
            zug = zug + 1
            abgelegt = True
            break
    if not abgelegt:
        if empty(stapel):
            stapel = haufen
            haufen = []
            k = pull(stapel)
            haufen.append(k)
            print("Stapel umgedreht.")

        karte = pull(stapel)
        spieler[zug % 3].append(karte)
        print("Spieler " + str(zug % 3 + 1) + " hat Karte " + karte + " gezogen.")
        zug = zug + 1

print("Haufen: ", haufen)
print(spieler)